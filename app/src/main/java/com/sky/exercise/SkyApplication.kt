package com.sky.exercise

import android.app.Application
import android.content.res.Configuration
import android.util.Log
import com.sky.exercise.service.SkyService
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.Cache
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class SkyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        val cache = Cache(cacheDir, (1024 * 1024 * 10).toLong())
        val builder = OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .cache(cache)

        val okHttpClient = builder.build()

        Picasso.setSingletonInstance(Picasso.Builder(this)
                .downloader(OkHttp3Downloader(okHttpClient))
                .build())
    }

    override fun onTerminate() {
        super.onTerminate()
        SkyService.getInstance().cancelRequests()
    }

    @Override
    override fun onConfigurationChanged(newConfig: Configuration?) {
        Log.d("tag", "config changed")
        super.onConfigurationChanged(newConfig)

        val orientation = newConfig?.orientation;
        when (orientation) {
            Configuration.ORIENTATION_PORTRAIT -> {
                Log.d("tag", "Portrait")
                vertical = true
            }
            Configuration.ORIENTATION_LANDSCAPE -> {
                Log.d("tag", "Landscape")
                vertical = false
            }
            else -> Log.w("tag", "other: $orientation")
        }
    }

    companion object {
        var vertical: Boolean = true
    }

}
