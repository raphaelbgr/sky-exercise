package com.sky.exercise

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import butterknife.ButterKnife
import com.sky.exercise.service.Movie
import com.sky.exercise.service.MoviesViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var moviesViewModel: MoviesViewModel
    private lateinit var adapter: MoviesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        init()
        getData()
    }

    private fun init() {
        moviesViewModel = ViewModelProviders.of(this).get(MoviesViewModel::class.java)
        moviesViewModel.moviesLiveData.observe(this, Observer<List<Movie>> { it?.let { it1 -> this.processResponseData(it1) } })
        moviesViewModel.errorLiveData.observe(this, Observer<String> { it?.let { it1 -> this.processError(it1) } })
    }

    private fun getData() {
        progressBar.visibility = View.VISIBLE
        moviesViewModel.getMoviesData()
    }

    private fun processResponseData(movies: List<Movie>) {
        progressBar.visibility = View.GONE
        textViewMainScreenTitle.visibility = View.VISIBLE
        recyclerView.visibility = View.VISIBLE
        adapter = MoviesAdapter(movies)
        val layoutManager = GridLayoutManager(this, if (SkyApplication.vertical) 2 else 3)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)
    }

    private fun processError(errorString: String) {
        progressBar.visibility = View.GONE
        textViewMainScreenTitle.visibility = View.GONE
        recyclerView.visibility = View.GONE
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.error))
                .setMessage(errorString)
                .setCancelable(false)
                .setNegativeButton(getText(R.string.close_app)) { dialog, _ ->
                    dialog.dismiss()
                    finish()
                }
                .setPositiveButton(getString(R.string.try_again)
                ) { _, _ -> getData() }
                .show()
    }

}
