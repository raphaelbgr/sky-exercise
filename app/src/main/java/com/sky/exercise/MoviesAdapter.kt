package com.sky.exercise

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.sky.exercise.service.Movie
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_view_holder.view.*


internal class MoviesAdapter(private val movieList: List<Movie>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MovieViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.movie_view_holder, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MovieViewHolder && movieList!![position].isValid) {
            holder.bind(movieList[position])
        }
    }

    override fun getItemCount(): Int {
        return movieList?.size ?: 0
    }

    internal class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(movie: Movie) {
            itemView.textViewMovieTitle.text = movie.title
            itemView.progressBar.visibility = View.VISIBLE
            Picasso.get()
                    .load(movie.coverUrl)
                    .error(R.drawable.imagem_indisponivel_sky)
                    .fit()
                    .into(itemView.imageViewCoverPicture, object : Callback {
                        override fun onSuccess() {
                            itemView.progressBar.visibility = View.GONE
                        }

                        override fun onError(e: Exception) {
                            itemView.progressBar.visibility = View.GONE
                            itemView.imageViewCoverPicture.scaleType = ImageView.ScaleType.FIT_XY
                        }
                    })
        }
    }

}
