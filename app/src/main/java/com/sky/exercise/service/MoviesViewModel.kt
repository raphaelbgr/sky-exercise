package com.sky.exercise.service

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException

class MoviesViewModel : ViewModel() {

    val moviesLiveData = MutableLiveData<List<Movie>>()
    val errorLiveData = MutableLiveData<String>()


    fun getMoviesData() {
        moviesLiveData.value?.let {
            onGetReservations(moviesLiveData.value)
        } ?: run {
            val disposable = SkyService.getInstance().api.movies
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .cache()
                    .subscribe({ onGetReservations(it) },
                            { handleError(it) })
            SkyService.getInstance().compositeDisposable.add(disposable)
        }
    }

    private fun onGetReservations(movie: List<Movie>?) {
        if (movie == null || movie.isEmpty()) {
            errorLiveData.postValue("Não há filmes disponíveis.")
        } else {
            moviesLiveData.postValue(movie)
        }
    }

    private fun handleError(e: Throwable) {
        if (e is IOException) {
            errorLiveData.postValue("Erro de comunicação. Por favor, tente novamente.")
        } else {
            errorLiveData.postValue("Erro de serviço. Por favor, tente novamente.")
        }
    }

}
