package com.sky.exercise.service

import io.reactivex.Flowable
import retrofit2.http.GET

interface MoviesApi {

    @get:GET("api/Movies")
    val movies: Flowable<List<Movie>>
}
