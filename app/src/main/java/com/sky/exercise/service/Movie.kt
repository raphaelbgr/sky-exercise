package com.sky.exercise.service

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Movie {

    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("overview")
    @Expose
    var overview: String? = null

    @SerializedName("duration")
    @Expose
    var duration: String? = null

    @SerializedName("release_year")
    @Expose
    var releaseYear: String? = null

    @SerializedName("cover_url")
    @Expose
    var coverUrl: String? = null

    @SerializedName("backdrops_url")
    @Expose
    var backdropsUrl: List<String>? = null

    @SerializedName("id")
    @Expose
    var id: String? = null

    val isValid: Boolean
        get() = (title != null && !title!!.isEmpty()
                && coverUrl != null && !coverUrl!!.isEmpty())

    /**
     * No args constructor for use in serialization
     */
    constructor() {}

    /**
     * @param id
     * @param backdropsUrl
     * @param duration
     * @param title
     * @param cover_url
     * @param overview
     * @param release_year
     */
    constructor(title: String, overview: String, duration: String, release_year: String, cover_url: String, backdropsUrl: List<String>, id: String) : super() {
        this.title = title
        this.overview = overview
        this.duration = duration
        this.releaseYear = release_year
        this.coverUrl = cover_url
        this.backdropsUrl = backdropsUrl
        this.id = id
    }

    fun withTitle(title: String): Movie {
        this.title = title
        return this
    }

    fun withOverview(overview: String): Movie {
        this.overview = overview
        return this
    }

    fun withDuration(duration: String): Movie {
        this.duration = duration
        return this
    }

    fun withReleaseYear(release_year: String): Movie {
        this.releaseYear = release_year
        return this
    }

    fun setCover_url(coverUrl: String) {
        this.coverUrl = coverUrl
    }

    fun withCoverUrl(coverUrl: String): Movie {
        this.coverUrl = coverUrl
        return this
    }

    fun withBackdropsUrl(backdropsUrl: List<String>): Movie {
        this.backdropsUrl = backdropsUrl
        return this
    }

    fun withId(id: String): Movie {
        this.id = id
        return this
    }

    override fun toString(): String {
        return "Movie{" +
                "title='" + title + '\''.toString() +
                ", overview='" + overview + '\''.toString() +
                ", duration='" + duration + '\''.toString() +
                ", releaseYear='" + releaseYear + '\''.toString() +
                ", coverUrl='" + coverUrl + '\''.toString() +
                ", backdropsUrl=" + backdropsUrl +
                ", id='" + id + '\''.toString() +
                '}'.toString()
    }

}
