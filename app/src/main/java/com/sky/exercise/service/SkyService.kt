package com.sky.exercise.service

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.sky.exercise.BuildConfig
import io.reactivex.disposables.CompositeDisposable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class SkyService {

    lateinit var compositeDisposable: CompositeDisposable
    lateinit var api: MoviesApi


    init {
        initApi()
    }

    fun initApi(url: String) {
        SkyService.url = url
        initApi()
    }

    private fun initApi() {
        compositeDisposable = CompositeDisposable()

        val gson = GsonBuilder()
                .setPrettyPrinting()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .setDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
                .serializeNulls()
                .create()

        api = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(OkHttpClient()).baseUrl(url)
                .build().create<MoviesApi>(MoviesApi::class.java)
    }

    fun cancelRequests() {
        compositeDisposable.clear()
    }


    companion object {
        var url: String = BuildConfig.MOVIES_URL
        private val instance: SkyService = SkyService()

        @Synchronized
        fun getInstance(): SkyService {
            return instance
        }
    }


}
