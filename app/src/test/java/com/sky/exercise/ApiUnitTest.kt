package com.sky.exercise

import com.sky.exercise.service.Movie
import com.sky.exercise.service.MoviesApi
import io.reactivex.Flowable
import io.reactivex.subscribers.TestSubscriber
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.io.IOException
import java.util.*

class ApiUnitTest {

    @Mock
    private val moviesApi: MoviesApi? = null

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun shouldGetMoviesWithSuccess() {
        val movieList = createMovieList()
        `when`(moviesApi!!.movies)
                .then { invocation -> Flowable.just(movieList) }

        val flowable = moviesApi.movies
        val testSubscriber = TestSubscriber<List<Movie>>()
        flowable.subscribe(testSubscriber)

        testSubscriber.assertNoErrors()
        val movieResult = flowable.blockingFirst()
        assertEquals(movieResult, movieList)
    }

    @Test
    fun shouldGetMoviesWithFailure() {
        `when`(moviesApi!!.movies)
                .then { invocation -> Flowable.error<Any>(IOException("HTTP_IO_ERROR")) }

        val flowable = moviesApi.movies
        val testSubscriber = TestSubscriber<List<Movie>>()
        flowable.subscribe(testSubscriber)

        val errors = testSubscriber.errors()
        val throwable = errors[0]
        Assert.assertEquals(throwable.message, "HTTP_IO_ERROR")
    }

    private fun createMovieList(): List<Movie> {
        val movies = ArrayList<Movie>()
        for (i in 0..11) {
            movies.add(Movie(String.format("Movie %s", i),
                    String.format("Overview %s", i),
                    String.format("Duration %s min", i),
                    String.format("Release Year %s", i),
                    String.format("Url %s", i),
                    ArrayList(),
                    String.format("id %s", i)))
        }
        return movies
    }
}