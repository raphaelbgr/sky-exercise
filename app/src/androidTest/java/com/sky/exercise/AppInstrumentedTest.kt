package com.sky.exercise

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import br.com.concretesolutions.requestmatcher.InstrumentedTestRequestMatcherRule
import com.sky.exercise.service.SkyService
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AppInstrumentedTest {

    @get:Rule
    val server = InstrumentedTestRequestMatcherRule()

    @get:Rule
    @JvmField
    val rule = ActivityTestRule<MainActivity>(MainActivity::class.java, false, false)

    fun robots(func: AppInstrumentedTestRobot.() -> Unit) = AppInstrumentedTestRobot(rule, server).apply(func)

    @Before
    fun baseSetUp() {
        SkyService.getInstance().initApi(server.url("/").toString())
    }

    @Test
    fun mainRecyclerShouldAppear() {
        robots {
            setupSuccessRequest()
            rule.launchActivity(null)
            validateSuccessScreen()
        }
    }

    @Test
    fun errorDialogShouldAppear() {
        robots {
            setupDisconnectRequest()
            rule.launchActivity(null)
            validateFailureScreen()
        }
    }
}
