package com.sky.exercise

import android.support.test.espresso.Espresso
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import br.com.concretesolutions.requestmatcher.RequestMatcherRule
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.SocketPolicy

class AppInstrumentedTestRobot(val rule: ActivityTestRule<MainActivity>,
                               val server: RequestMatcherRule) {

    fun setupSuccessRequest() {
        server.addFixture("movies_ok.json")
                .ifRequestMatches()
                .pathIs("/api/Movies")
    }

    fun validateSuccessScreen() {
        Espresso.onView(ViewMatchers.withText("Cine SKY"))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withText("Test Movie 001"))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withText("Test Movie 002"))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    fun setupDisconnectRequest() {
        server.addResponse(MockResponse().setSocketPolicy(SocketPolicy.DISCONNECT_AT_START))
                .ifRequestMatches()
                .pathIs("/api/Movies")
    }

    fun validateFailureScreen() {
        Espresso.onView(ViewMatchers.withText("Erro"))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withText("Erro de comunicação. Por favor, tente novamente."))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withText("TENTAR NOVAMENTE"))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withText("FECHAR APP"))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

}